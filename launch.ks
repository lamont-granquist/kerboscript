@LAZYGLOBAL off.

run once lib_gravity_turn_v3.
run once lib_misc.
run once lib_utils.

set terminal:width to 80.
set terminal:height to 80.

clearscreen.

wait until ship:unpacked.
CORE:PART:GETMODULE("kOSProcessor"):DOEVENT("Open Terminal").

// reduce the wiggles
SET STEERINGMANAGER:PITCHTORQUEADJUST TO 20.
SET STEERINGMANAGER:YAWTORQUEADJUST TO 20.
SET STEERINGMANAGER:ROLLTORQUEADJUST TO 20.

if status = "prelaunch" or status = "landed" {
//  autojettison_fairings().
  autodeploy_antennas().

  gravity_turn_v3().
}
