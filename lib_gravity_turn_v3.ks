// RO/RSS gravity turner

run once lib_misc.

function gravity_turn_v3 {
  parameter inclination is 28.3626779079849.
  parameter start_speed is 50.
  parameter start_turn is 6.1.
  parameter target_alt is 161000.
  parameter fadeout is 0.024.
  parameter max_aoa is 3.

  // ============================
  // start attitude state machine
  // ============================

  function attitude_launch {
    if velocity:surface:mag > start_speed {
      print "initiating gravity turn".
      return attitude_surf().
    }
    set launch_pitch to 90.
    return attitude_launch@.
  }

  function attitude_surf {
    if ship:q < ( max_q / 2 ) and max_q > 0.05 and ship:q < fadeout {
      print "dynamic fadeout".
      return attitude_fadeout().
    }
    local surf_pitch to 90 - arccos(vdot(velocity:surface:normalized, ship:up:vector:normalized)).
    set launch_pitch to max(0, min(90-start_turn, surf_pitch + max_aoa)).
    return attitude_surf@.
  }

  function attitude_fadeout {
    // if eta:apoapsis > eta:periapsis {
    //  return attitude_park_computation@.
    //}

    // FIXME: should allow pitching up here
    local surf_pitch to 90 - arccos(vdot(velocity:surface:normalized, ship:up:vector:normalized)).
    local fade to min(fadeout / ship:q * max_aoa, 90).

    if ship:apoapsis < target_alt
      set launch_pitch to max(0, surf_pitch + max_aoa).
    else
      set launch_pitch to max(0, surf_pitch - max_aoa).

    return attitude_fadeout@.
  }

  function attitude_park_computation {
    local deltav to stage_deltav().
    local burntime to burntime(deltav).
    local x0 to x_at(ship, burntime/2).
    local v0 to v_at(ship, burntime/2).
    return attitude_park@.
  }

  function attitude_park {
    return attitude_park@.
  }

  // ===========================
  // start staging state machine
  // ===========================

  local last_stage_time to 0.

  function do_stage {
    set last_stage_time to time:seconds.
    stage.
  }

  function stage_not_ready {
    if not stage:ready
      return true.
    if time:seconds - last_stage_time < 1
      return true.
    return false.
  }

  function staging_last {
    return staging_last@.
  }

  function staging_fairings {
    if stage_not_ready()
      return staging_fairings@.

    jettison_fairings().

    return staging_last@.
  }

  function staging_ullage {
    if stage_not_ready()
      return staging_ullage@.

    local l to list().
    list engines in l.
    for e in l {
      if e:stage = stage:number - 1 {
        // FIXME: how to check for stable propellant???
        do_stage().
        return staging_fairings@.
      }
    }

    return staging_ullage@.
  }

  function staging_cutout {
    if stage_not_ready()
      return staging_cutout@.

    if maxthrust = 0 {
      print "staging after maxthrust = 0".
      do_stage().
      return staging_ullage@.
    }

    return staging_cutout@.
  }

  function staging_spoolup {
    if stage_not_ready()
      return staging_spoolup@.

    local l to list().
    list engines in l.

    for e in l
      if e:ignition and ( e:thrust / e:availablethrust < 0.98 )
        return staging_spoolup@.

    print "staging after spoolup".
    do_stage().
    return staging_cutout@.
  }

  function staging_launch {
    if stage_not_ready()
      return staging_launch@.

    print "staging to launch".
    do_stage().
    return staging_spoolup@.
  }

  // =========
  // main body
  // =========

  lock throttle to 1.

  lock launch_dir to heading_for_launch_inclination(inclination, target_alt).
  local launch_pitch to 90.
  lock launch_heading to heading(launch_dir,launch_pitch).

  lock steering to R(launch_heading:pitch, launch_heading:yaw, 180).

  start_max_q_tracking().

  set attitude_state to attitude_launch@.

  set staging_state to staging_launch@.

  until attitude_state = staging_last@ and maxthrust = 0 {
    // attitude control
    set attitude_state to attitude_state:call().
    set staging_state to staging_state:call().

    if attitude_state = "stop" or staging_state = "stop"
      break.

    wait 0.
  }

  stop_max_q_tracking().

  unlock throttle.
  set ship:control:pilotmainthrottle to 0.
}
