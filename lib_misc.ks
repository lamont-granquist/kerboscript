@LAZYGLOBAL off.
run once lib_utils.

function jettison_fairings {
  print "jettisoning all fairings".
  local m1 to ship:modulesnamed("ModuleProceduralFairing").
  local m2 to ship:modulesnamed("ProceduralFairingDecoupler").
  for m in ship:modulesnamed("ModuleProceduralFairing") {
    m:doevent("deploy").
  }
  for m in ship:modulesnamed("ProceduralFairingDecoupler") {
    m:doevent("jettison").
  }
}

global max_q to 0.
global tracking_max_q to false.

function start_max_q_tracking {
  set tracking_max_q to true.
  when tracking_max_q then {
    set max_q to max(ship:q, max_q).
    preserve.
  }
}

function stop_max_q_tracking {
  set tracking_max_q to false.
}

function autojettison_fairings {
  when ship:q < (max_q / 2) and ship:q < 0.005 and max_q > 0.05 then {
    jettison_fairings().
  }
}

function deploy_antennas {
  print "deploying antennas".
  for m in ship:modulesnamed("ModuleRTAntenna") {
    if m:hasevent("activate") {
      m:doevent("activate").
    }
  }
}

function autodeploy_antennas {
  when ship:altitude > body:atm:height then {
    deploy_antennas().
  }
}

function autostow_panels {
  when ship:altitude < body:atm:height then {
    print "stowing solar panels".
    panels off.
    autodeploy_panels().
  }
}

function autodeploy_panels {
  when ship:altitude > body:atm:height then {
    print "deploying solar panels".
    panels on.
    autostow_panels().
  }
}

function activate_engines {
  local l to list().
  list engines in l.
  for e in l
    e:activate().
}

function flight {
  if not stage:ready
    return false.

  if maxthrust = 0 {
    print "staging after maxthrust 0".
    return true.
  }

  local l to list().
  list engines in l.
  for e in l
    if e:flameout {
      // skip flamed out ullage solid motors
      for r in e:resources
        if r:name = "PSPC" and r:capacity < 1000
          return false.
      print "staging after engine flameout".
      return true.
    }
  return false.
}

function spoolup {
  local l to list().
  list engines in l.
  for e in l
    if e:ignition and ( e:thrust / e:availablethrust < 0.98 )
      return false.
  set auto_stage_check to flight@.
  print "staging after spoolup".
  return true.
}

function liftoff {
  set auto_stage_check to spoolup@.
  print "staging for liftoff".
  return true.
}

global auto_stage_check to liftoff@.
global auto_stage_maxstage to 0.

function auto_stage {
  parameter maxstage is 0.

  set auto_stage_maxstage to maxstage.
  set auto_stage_check to liftoff@.

  // ISSUE:  since we're doing this in a when/then block, it
  // becomes impossible to "wait 2" when maxthrust = 0 to give
  // bottom stages time to separate and not be explodey.
  when auto_stage_check:call() then {
    stage.
    if stage:number > auto_stage_maxstage
      preserve.
  }
}

// global kerbin_sidereal_day to 5*3600 + 59*60 + 9.4.
// global keosynch_radius to (sqrt(kerbin:mu) * kerbin_sidereal_day / (2 * constant:pi))^(2/3).  // 3463331.36.
// global keosynch_altitude to keosynch_radius - kerbin:radius.

function keosynch_orbit {
  parameter ksc_angle is 0.

  local sidereal_day to 5*3600 + 59*60 + 9.4.

  // x0, v0 are what we use to build the orbit
  local x0 to latlng(0, clamp_degrees_180(-75.08333333333333333332 + ksc_angle)):position - ship:body:position.

  set x0:mag to keosynch_radius.
  local v0 to vcrs(kerbin:angularvel, x0).
  set v0:mag to sqrt(kerbin:mu / x0:mag).

  return orbit(x0 + ship:body:position, v0, ship:body, time:seconds).
}

function closest_vessel {
  local targets to list().
  list targets in l.
  local lo to 99999999999.
  local ret to "none".
  for t in l {
    if l:position:mag < lo {
      set lo to l:position:mag.
      set ret to l.
    }
  }
  if ret <> "none"
    return ret.

  // FIXME: need to be able to `list vessels in l` to get all of them.
  print "couldn't find a vessel".
  exit.
}

function raise_apoapsis {
  parameter target_alt.

  if ship:apoapsis < target_alt {
    set warp to 0.
    wait 1.
    until ship:apoapsis > target_alt
      lock throttle to 0.05.
  }
  lock throttle to 0.
}

function coast_to_atm_edge {

  lock throttle to 0.
  lock steering to prograde.

  wait until vang(facing:vector, prograde:forevector) < 1.

  wait until ship:altitude > body:atm:height.

  unlock throttle.
  set ship:control:pilotmainthrottle to 0.
}
