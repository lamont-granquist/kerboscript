### explosions

to see if something just exploded and revert if it did -- check to see if the ship length changed, and we didn't just stage.

this will false positive on a manual undock though.

```
on ship:parts:length {
  if stage:ready kuniverse:reverttolaunch().
    return true.
}
```

### launch code for boot

```
if status = "prelaunch" or status = "landed" { launch(). }
```

### overall state machine

storing the current "phase" in `core:part:tag

### split savegame into two to do stage recovery

http://forum.kerbalspaceprogram.com/index.php?/topic/72605-110-flight-manager-for-reusable-stages-fmrs-x110-experimental/

### debugging instructions

output debugging to the debug.log

```
set config:debugeachopcode to true
``

