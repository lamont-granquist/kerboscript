@LAZYGLOBAL off.

run once lib_gravity_turn_v2.
run once lib_node.
run once lib_misc.
run once lib_utils.

set terminal:width to 80.
set terminal:height to 80.

wait until ship:unpacked.
CORE:PART:GETMODULE("kOSProcessor"):DOEVENT("Open Terminal").

local target_alt to 80000.

autojettison_fairings().
auto_stage(2).

if status = "prelaunch" or status = "landed" {
  gravity_turn_v2(target_alt, 0, target_alt).
}

print "coasting to edge of atm".

coast_to_atm_edge().

print "raising atm if we need to".

raise_apoapsis(target_alt).

print "computing circ burn".

node_circularize_time(eta:apoapsis).

print "executing circ burn".

node_execute().

print "computing inclination change".

node_inclination_time(eta_andn(), 0).

print "executing inclination change".

node_execute().

print "computing keosynch orbit".

local keo to keosynch_orbit().

print "computing hohmann transfer to keo".

node_hohmann_obt(keo).

print "executing transfer to keo".

node_execute().

print "raising apoapsis above " + keosynch_altitude.

lock steering to prograde:forevector.

lock throttle to 0.2.

wait until apoapsis > keosynch_altitude.

lock throttle to 0.

wait 0.5.

print "warping to apoapsis".

warpto(time:seconds + eta:apoapsis - 120).
wait until warp = 0.

node_period_time(0, 0.75 * keo:period).

node_execute().

lock steering to prograde.

wait until vang(ship:facing:vector, prograde:vector) < 0.5.

function decouple {
  local m to list().

  for m in ship:modulesnamed("ModuleDecouple") {
    if m:hasevent("Decouple") {
      m:doevent("Decouple").
      break.
    }
  }
}

decouple().

wait 60.

wait until eta:periapsis < eta:apoapsis.

warpto(time:seconds + eta:apoapsis).

wait until warp = 0.

decouple().

wait 60.

wait until eta:periapsis < eta:apoapsis.

warpto(time:seconds + eta:apoapsis).

wait until warp = 0.

decouple().

wait 60.

wait until eta:periapsis < eta:apoapsis.

warpto(time:seconds + eta:apoapsis).

wait until warp = 0.

decouple().

print "end".
