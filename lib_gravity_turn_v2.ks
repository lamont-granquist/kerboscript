// better Gravity Turner

run once lib_misc.

function gravity_turn_v2 {
  parameter target_alt.   // FIXME: as the target_alt increases, we need to raise the intermediate_alt or we go too fast/too low + burn up
  parameter inclination is 90.
  parameter intermediate_alt is 60000.
  parameter start_speed is 50.
  parameter start_turn is 16.

  // ============================
  // start attitude state machine
  // ============================

  function attitude_launch {
    if velocity:surface:mag > start_speed {
      print "initiating gravity turn".
      return attitude_initiate().
    }
    // FIXME: maybe locking will work now?  definitely extract it
    set launch_heading to heading_for_launch_inclination(inclination, target_alt).
    set launch_pitch to 90.
    return attitude_launch@.
  }

  function attitude_initiate {
    if vang(facing:vector, velocity:surface) > 2 {
      print "settling turn".
      return attitude_settle().
    }
    set launch_heading to heading_for_launch_inclination(inclination, target_alt).
    set launch_pitch to 90 - start_turn.
    return attitude_initiate@.
  }

  function attitude_settle {
    if vang(facing:vector, velocity:surface) < 1 {
      print "tracking surface vector".
      return attitude_surf().
    }
    set launch_heading to heading_for_launch_inclination(inclination, target_alt).
    set launch_pitch to 90 - start_turn.
    return attitude_settle@.
  }

  function attitude_surf {
    if ship:q < 0.004 and ship:q < (max_q / 2) and max_q > 0.05 {
      print "tracking flat attitude for intermediate burn".
      return attitude_flat().
    }
    set launch_heading to heading_for_launch_inclination(inclination, target_alt).
    set launch_pitch to 90 - arccos(vdot(velocity:surface:normalized, ship:up:vector:normalized)).
    return attitude_surf@.
  }

  function attitude_flat {
    if ship:altitude >= 1.1 * intermediate_alt {
      print "tracking prograde".
      return attitude_prograde().
    }
    set launch_heading to heading_for_launch_inclination(inclination, target_alt).
    set launch_pitch to 90 - arccos(vdot(prograde:vector:normalized, ship:up:vector:normalized)).
    return attitude_flat@.
//    return list(attitude_flat@, vxcl(ship:up:vector, ship:velocity:surface)).
  }

  function attitude_prograde {
    return list(attitude_prograde@, prograde).
  }

  // ======================
  // throttle state machine
  // ======================

  function throttle_launch {
    if ship:apoapsis > intermediate_alt {
      if ship:apoapsis > target_alt {
        print "ending gravity turn early".
        return "stop".
      } else {
        print "coasting to intermediate altitude".
        return throttle_coast1().
      }
    }
    //FIXME: better maxQ PID clamp
    if ship:q > 0.4 {
      return list(throttle_launch@, 0.5).
    } else {
      return list(throttle_launch@, 1.0).
    }
  }

  function throttle_coast1 {
    if eta:apoapsis < 20 {
      print "starting horizontal burn".
      return throttle_horizontal().
    }
    if ship:apoapsis < intermediate_alt {
      return list(throttle_coast1@, 0.2).
    } else {
      return list(throttle_coast1@, 0.0).
    }
  }

  function throttle_horizontal {
    if ship:apoapsis < target_alt {
      return list(throttle_horizontal@, 1.0).
    } else {
      return list(throttle_horizontal@, 0.0).
    }
  }

  // =========
  // main body
  // =========

  local launch_heading to heading_for_launch_inclination(inclination, target_alt).
  local launch_pitch to 90.

  local th to 1.
  lock throttle to th.

  // why does R have to be first here?
  lock steering to R(0, 0, -90) + heading(launch_heading,launch_pitch).

  start_max_q_tracking().

  set attitude_state to attitude_launch@.
  set throttle_state to throttle_launch@.

  until ship:apoapsis > target_alt {
    set ret to throttle_state:call().
    set throttle_state to ret[0].

    if throttle_state = "stop"
      break.

    set th to ret[1].

    // attitude control
    set attitude_state to attitude_state:call().

    if attitude_state = "stop"
      break.

    wait 0.
  }

  stop_max_q_tracking().

  unlock throttle.
  set ship:control:pilotmainthrottle to 0.
}
