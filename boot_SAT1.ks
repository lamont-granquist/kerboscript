@LAZYGLOBAL off.
if status = "prelaunch" or status = "landed" {
  switch to 0.
  copy comsat_1.ks to 1.
  copy lib_misc to 1.
  copy lib_node to 1.
  copy lib_utils to 1.

  list files.
}

print "Boot script for comsat 1".
run comsat_1.
