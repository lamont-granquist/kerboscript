@LAZYGLOBAL off.

run once lib_node.
run once lib_misc.
run once lib_utils.

wait until ship:unpacked.

function only_processor {
  local procs to list().
  list processors in procs.
  return procs:length() = 1.
}

until only_processor {
  wait 1.
}

set terminal:width to 80.
set terminal:height to 80.

CORE:PART:GETMODULE("kOSProcessor"):DOEVENT("Open Terminal").

set kuniverse:activevessel to ship.

panels on.

deploy_antennas().

activate_engines().

wait 0.5.

node_circularize_time(eta_altitude(keosynch_altitude)).

node_execute().

set kuniverse:activevessel to closest_vessel().
