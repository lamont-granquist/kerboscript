// kOS implementation of Gravity Turn style ascents
//  - does not pitch up for low TWR upper stages
//  - does not compensate for Ap overshoot for SRBs

function gravity_turn {
  parameter target_alt.
  parameter start_speed is 50.
  parameter start_turn is 16.
  parameter time_to_ap is 40.
  parameter kp is 0.1.
  parameter ki is 0.01.
  parameter kd is 0.05.

  local max_q to 0.

  // ============================
  // start attitude state machine
  // ============================

  function state_launch {
    if velocity:surface:mag > start_speed {
      print "initiating gravity turn".
      return state_initiate().
    }
    return list(state_launch@, heading(90,90)).
  }

  function state_initiate {
    if vang(facing:vector, velocity:surface) > 2 {
      print "settling turn".
      return state_settle().
    }
    return list(state_initiate@, heading(90,90-start_turn)).
  }

  function state_settle {
    if vang(facing:vector, velocity:surface) < 1 {
      print "tracking surface vector".
      return state_surf().
    }
    return list(state_settle@, heading(90,90-start_turn)).
  }

  function state_surf {
    set max_q to max(ship:q, max_q).

    if ((ship:q * constant:atmtokpa) < 2.5) and (ship:q < (max_q / 2)) {
      print "tracking prograde vector".
      return state_prograde().
    }
    return list(state_surf@, velocity:surface).
  }

  function state_prograde {
    return list(state_prograde@, prograde).
  }

  // ==========================
  // end attitude state machine
  // ==========================

  local th to 1.
  lock throttle to th.
  lock steering to heading(90,90).

  set state to state_launch@.

  set pid_throttle to pidloop(kp, ki, kd, -0.2, 0.2).
  set pid_throttle :setpoint to time_to_ap.

  until ship:apoapsis > target_alt {

    // throttle control

    if eta:apoapsis > eta:periapsis
      // past the Ap, heading down, low TWR upper stage, FIXME: should also pitch up
      local input to time_to_ap + 100.
    else
      local input to eta:apoapsis.
    set output to pid_throttle:update(time:seconds, input).
    // min clamp is used to push Ap up to target, cannot be zero.
    set th to min(1, max(0.1, th + output)).

    // attitude control

    set ret to state:call().
    set state to ret[0].
    lock steering to ret[1].

    wait 0.001.
  }

  unlock throttle.
  set ship:control:pilotmainthrottle to 0.
}
