//////////
// engines
//////////

// will in general be confused by asparagus staging and solid boosters
// also will be confused if the resources don't burn down to zero (i.e. run out of oxidizer before fuel or vice versa or whatever)
// i removed PSPC, and should remove any other solid fuels
function stage_deltav {
  local fueltypes to list("LQDOXYGEN", "LQDHYDROGEN", "KEROSENE", "Aerozine50", "UDMH", "NTO", "MMH", "HTP", "NitrousOxide", "Aniline", "Ethanol75", "LQDAMMONIA", "LQDMETHANE", "CLF3", "CLF5", "DIBORANE", "PENTABORANE", "ETHANE", "ETHYLENE", "OF2", "LQDFLUORINE", "N2F4", "FurFuryl", "UH25", "TONKA250", "TONKA500", "FLOX30", "FLOX70", "FLOX88", "IWFNA", "AK20", "AK27", "CaveaB", "MON1", "MON3", "MON10", "MON15", "MON20", "Hydyne", "IRFNA-III", "IRFNA-IV", "LIQUIDFUEL", "OXIDIZER").
  local fuel_mass to 0.
  for r in stage:resources
    if fueltypes:contains(r:name)
      set fuel_mass to fuel_mass + r:amount * r:density.
  return 9.8 * g_isp * ln(ship:mass / (ship:mass - fuel_mass)).
}

function thrust_to_weight {
  // FIXME: unimplemented.
}

function thrust_to_mass {
  return g_maxthr / mass.
}

// global helper vars so we only calculate isp + thrust once per update
lock g_thrustisp to engine_isp_thrust_sum().
lock g_isp to g_thrustisp[0].
lock g_maxthr to g_thrustisip[1].

// sum over all engines to get the current thrust + isp
// return value is list(<avgisp>, <maxthrust>)
function engine_isp_thrust_sum {
  local m is 0.
  local t is 0.

  list engines in l.
  for e in l
    if e:ignition {
      local th is e:maxthrust*e:thrustlimit/100.
      set t to t + th.
      if e:visp = 0
        set m to 1.
      else
        set m to m+th/e:visp.
    }
  if m = 0
    return list(0, 0).
  else
    return list(t/m, t).
}

///////
// ETAs
///////

function eta_an {
  return eta_true(clamp_degrees_360(-ship:orbit:argumentofperiapsis + 180)).
}

function eta_dn {
  return eta_true(clamp_degrees_360(-ship:orbit:argumentofperiapsis)).
}

function eta_andn {
  print "ETA DN: " + eta_dn().
  print "ETA AN: " + eta_an().
  return min(eta_an(), eta_dn()).
}

function eta_altitude {
  parameter altitude.

  if altitude > orbit:apoapsis {
    print "WARNING: altitude of " + altitude + " exceeds apopasis of " + orbit:apoapsis + " setting to eta:apoapsis!".
    return eta:apoapsis.
  }

  if altitude < orbit:periapsis {
    print "WARNING: altitude of " + altitude + " is below periapsis of " + orbit:periapsis + " setting to eta:periapsis!".
    return eta:periapsis.
  }

  local MA1 to mean_from_radius(ship:orbit, body:radius + altitude).

  return min( eta_mean(MA1), eta_mean(360 - MA1) ).
}

function eta_mean {
  parameter MA1.

  local MA0 to mean_from_orbit(ship:orbit).

  print "MA0: " + MA0.
  print "MA1: " + MA1.

  return sqrt( orbit:semimajoraxis^3 / orbit:body:mu ) * clamp_degrees_360( MA1 - MA0 ) * constant:degtorad.
}

function eta_true {
  parameter TA1.

  return eta_mean(mean_from_true(ship:orbit, TA1)).
}

////////////
// anomalies
////////////

// degrees time=now
function mean_from_orbit {
  parameter orbit.

  return mean_from_eccentric(orbit, eccentric_from_orbit(orbit)).
}

// degrees time=now
function eccentric_from_orbit {
  parameter orbit.

  return eccentric_from_true(orbit, orbit:trueanomaly).
}

// degrees
function mean_from_true {
  parameter orbit.
  parameter TA.

  return mean_from_eccentric(orbit, eccentric_from_true(orbit, TA)).
}

// degrees
function mean_from_radius {
  parameter orbit.
  parameter radius.

  return mean_from_eccentric(orbit, eccentric_from_radius(orbit, radius)).
}

// degrees
function mean_from_eccentric {
  parameter orbit.
  parameter EA.

  return clamp_degrees_360( ( EA * constant:degtorad - orbit:eccentricity * sin(EA) ) * constant:radtodeg ).
}

// degrees
function eccentric_from_true {
  parameter orbit.
  parameter TA.

  local ecc to orbit:eccentricity.

  return clamp_degrees_360( arctan( sqrt(1 - ecc^2 ) * sin(TA) / ( ecc + cos(TA) ) ) ).
}

// degrees
function eccentric_from_radius {
  parameter orbit.
  parameter radius.

  return clamp_degrees_360( arccos( ( 1 - radius / orbit:semimajoraxis ) / orbit:eccentricity ) ).
}

///////////
// vis viva
///////////

function radius_from_vis_viva {
  parameter orbit.
  parameter velocity.

  return 2/(velocity^2/orbit:body:mu + 1/orbit:semimajoraxis).
}

function velocity_from_vis_viva {
  parameter orbit.
  parameter radius.

  return sqrt( orbit:body:mu * ( 2/radius - 1/orbit:semimajoraxis ) ).
}

//////////////
// inclination
//////////////

// degrees, returns kerbal headings 0 == north, 90 == east
function heading_for_inclination {
  parameter inclination.
  parameter latitude.

  local cos_heading to cos(inclination) / cos(latitude).

  if abs(cos_heading) > 1 {
    if abs(clamp_degrees_180(inclination)) < 90
      return 90.
    else
      return 270.
  }

  local angle_from_east to arccos(cos_heading).

  if inclination < 0
    set angle_from_east to - angle_from_east.

  return clamp_degrees_360(90 - angle_from_east).
}

// should work for planets with an inclined or reversed rotation!
function heading_for_launch_inclination {
  parameter inclination.
  parameter altitude.

  local heading to heading_for_inclination(inclination, ship:latitude).

  local v to heading(heading, 0):vector.
  set v:mag to sqrt(body:mu/(body:radius + altitude)).

  local v_horiz to vxcl(ship:up:vector, ship:velocity:orbit).
  local dir to v - v_horiz.

  // at the end of the launch dir can flip around violently
  if vdot(dir:normalized, v:normalized) < 0.90
    return heading.

  local angle to arccos(vdot(dir:normalized, north_at(ship, 0))).

  if inclination < 0
    set angle to 180 - angle.

  return clamp_degrees_360(angle).
}

// 0 to 359
function clamp_degrees_360 {
  parameter degrees.

  local r to mod(degrees, 360).

  if r >=- 0
    return r.
  else
    return r + 360.
}

// -179 to 180
function clamp_degrees_180 {
  parameter degrees.

  local r to clamp_degrees_360(degrees).

  if r > 180
    return r - 360.
  else
    return r.
}

///////////////////
// orbit prediction
///////////////////

function north_at {
  parameter orbitable.
  parameter dt.

  return vxcl(x_at(orbitable, dt):normalized, v(0,1,0)):normalized.
}

function east_at {
  parameter orbitable.
  parameter dt.

  return vcrs(x_at(orbitable, dt):normalized, north_at(orbitable,dt)):normalized.
}

function up_at {
  parameter orbitable.
  parameter dt.

  return vcrs(north_at(orbitable, dt), east_at(orbitable,dt)):normalized.
}

function v_at {
  parameter orbitable.
  parameter dt.

  return velocityat(orbitable, time:seconds+dt):orbit.
}

function x_at {
  parameter orbitable.
  parameter dt.

  return positionat(orbitable, time:seconds+dt) - orbitat(orbitable, time:seconds+dt):body:position.
}

function lat_at {
  parameter orbitable.
  parameter dt.

  return 90 - arccos(vdot(x_at(orbitable, dt):normalized, V(0, 1, 0))).
}


////////////////////
// numerical methods
////////////////////

// increases x_hi until it finds a crossing
function expand_range {
  parameter fun.
  parameter x_lo.
  parameter x_hi.
  parameter f to 1.5.  // 2 would be faster, but might overshoot?

  local fx_lo to fun(x_lo).
  local fx_hi to fun(x_hi).

  print "expand range x_lo : " + x_lo + " is : " + fx_lo.
  print "expand range x_hi : " + x_hi + " is : " + fx_hi.

  until ( fx_lo > 0 and fx_hi < 0 ) or ( fx_lo < 0 and fx_hi > 0 ) {
    set x_hi to x_hi * f.
    set fx_hi to fun(x_hi).
    print "expand range new hi : " + x_hi + " is : " + fx_hi.
  }

  return x_hi.
}

global secant_method_failed to false.

// a bit tweaked
function secant_method {
  parameter fun.
  parameter x_lo.
  parameter x_hi.
  parameter eta to 0.01.
  parameter max_i to 100.

  set secant_method_failed to false.

  local fx_lo to fun(x_lo).
  local fx_hi to fun(x_hi).

  if fx_lo > fx_hi {
    return secant_method(fun, x_hi, x_lo, eta, max_i).
  }

  if fx_lo > 0 {
    print "SECANT ERROR: both ends are positive".
    exit.
  }

  if fx_hi < 0 {
    print "SECANT ERROR: both ends are negative".
    exit.
  }

  from { local i to 1. } until i > max_i step { set i to i + 1. } do {
    local x to x_lo - fx_lo * ( x_lo - x_hi ) / ( fx_lo - fx_hi ).
    local fx to fun(x).

    if abs(fx) < eta
      return x.

    print "SECANT f(x): " + fx.

    // first two conditions handles perfectly flat functions
    if fx = fx_lo {
      print "FLAT LO".
      set x_lo to x.
      set fx_lo to fx.
    } else if fx = fx_hi {
      print "FLAT HI".
      set x_hi to x.
      set fx_hi to fx.
    // normal bracket shrinking
    } else if fx < 0 {
      print "LO".
      set x_lo to x.
      set fx_lo to fx.
    } else {
      print "HI".
      set x_hi to x.
      set fx_hi to fx.
    }
  }

  print "secant failed to converge!".
  set secant_method_failed to true.
}
