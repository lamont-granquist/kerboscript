run once lib_utils.

/////////////////////
// generic node utils
/////////////////////

// burntime calculator that compensates for mass loss using rocket eq.
//  - does not account for staging during the burn.
function burntime {
  parameter deltav.

  set res to engine_isp_thrust_sum().
  set ve to g_isp * 9.8.
  return ((mass*ve)/g_maxthr)*(1-constant:e^(-deltav/ve)).
}

function node_execute {
  // bigger ships may need more time.
  parameter settletime is 5.

  local th to 0.
  lock throttle to th.

  set node to nextnode.

  lock steering to node:deltav.

  wait until vang(ship:facing:vector, node:deltav) < 1.

  set warpmode to "rails".

  warpto(time:seconds + node:eta - burntime(nextnode:deltav:mag)/2 - settletime).

  until node:eta <= burntime(nextnode:deltav:mag)/2 + settletime {
    if vang(ship:facing:vector, node:deltav) > 5 {
      // BUG: this very clearly does not work
      set warp to 0.
      wait until vang(ship:facing:vector, node:deltav) < 1.
      warpto(time:seconds + node:eta - burntime(nextnode:deltav:mag)/2 - settletime).
    }
    wait 0.
  }

  wait until node:eta <= burntime(nextnode:deltav:mag)/2.

  set th to 1.

  until node:deltav:mag < 1 {
    if vang(ship:facing:vector, node:deltav) > 1 {
      set th to 0.
    } else {
      local tmr to thrust_to_mass().
      if tmr > 0 {
        set th to max(0.01, min(1, node:deltav:mag/tmr/2)).
      } else {
        // should probably bail out of this somehow, but might be staging.
        set th to 0.
      }
    }
  }

  set th to 0.
  wait until vang(ship:facing:vector, node:deltav) < 0.1.

  set th to 0.01.
  wait until vang(ship:facing:vector, node:deltav) > 90.
  set th to 0.

  // important or steering is locked to removed node
  unlock steering.
  unlock throttle.

  set ship:control:pilotmainthrottle to 0.

  remove node.
}

function node_from_vector {
  parameter dt.
  parameter vector.

  local v0 to v_at(ship, dt).
  local x0 to x_at(ship, dt).

  local unit_prograde to v0:normalized.
  local unit_normal to vcrs(v0:normalized, x0:normalized).
  local unit_radial to vcrs(unit_normal, unit_prograde).

  local prograde_burn to vdot(vector, unit_prograde).
  local normal_burn to vdot(vector, unit_normal).
  local radial_burn to vdot(vector, unit_radial).

  local n to node(time:seconds+dt, radial_burn, normal_burn, prograde_burn).

  add n.
  return n.
}


//////////////////
// circularization
//////////////////

function node_circularize_time {
  parameter dt.

  local v0 to v_at(ship, dt).
  local x0 to x_at(ship, dt).

  local v0_horiz to vxcl(x0:normalized, v0).
  local unit_horiz to v0_horiz:normalized.

  local v1 to sqrt(body:mu / x0:mag) * unit_horiz.

  return node_from_vector(dt, v1 - v0).
}

//////////
// Ap + Pe
//////////

// FIXME: merge with vis_viva utils
function velocity_from_alt_pe_ap {
  parameter body.
  parameter alt.
  parameter pe.
  parameter ap.

  local sma to ( pe + ap + 2*body:radius ) / 2.
  return sqrt( body:mu * ( 2/(alt+body:radius) - 1/sma ) ).
}

function perturbed_orbit {
  parameter v0.
  parameter x0.
  parameter body.
  parameter dv.

  local mu to body:mu.

  local v1 to v0 + dv.
  local h to vcrs(x0, v1).
  local eccvec to vcrs(v1, h) / mu - x0:normalized.
  local ecc to eccvec:mag.

  local mech_e to v1:mag^2 / 2 - mu / x0:mag.
  local sma to - mu / (2 * mech_e).

  local pa to ( 1 - ecc ) * sma - body:radius.
  local ap to ( 1 + ecc ) * sma - body:radius.

  return list(ecc, sma, pa, ap).
}

function guess {
  parameter v0.
  parameter x0.
  parameter body.
  parameter what.
  parameter tgt.
  parameter mag.

  local v0_horiz to vxcl(x0:normalized, v0).
  local dv to v0_horiz:vec.
  set dv:mag to mag.

  local ret to perturbed_orbit(v0, x0, body, dv).

  local pe1 to ret[2].
  local ap1 to ret[3].
  local period1 to 2 * constant:pi * sqrt( ret[1]^3 / body:mu ).

  // hack to keep the function continuous, although it makes it perfectly flat.
  if ap1 < 0
    set ap1 to body:soiradius + 1.

  // hack to keep the period function continuous, past when it goes hyperbolic
  if period1 < 0
    set period1 to 2 * constant:pi * sqrt( (2 *body:soiradius)^3 / body:mu ).

  if what = "ap" {
    return tgt - ap1.
  } else if what = "pe" {
    return tgt - pe1.
  } else if what = "period" {
    print "guess: " + mag + " = " + period1.
    return tgt - period1.
  } else {
    print "called guess with a bad what value".
    exit.
  }
}

function node_period_time {
  parameter dt.
  parameter period.

  local v0 to v_at(ship, dt).
  local x0 to x_at(ship, dt).
  local o to orbitat(ship, time:seconds + dt).
  local v0_horiz to vxcl(x0:normalized, v0).
  local v0_vert to vdot(v0, x0:normalized) * x0:normalized.

  local dv_max to 0.

  if period < o:period {
    set dv_max to - v0_horiz:mag.
  } else {
    set dv_max to expand_range(guess@:bind(v0, x0, body, "period", period), 0, 10).
  }

  local mag to secant_method(guess@:bind(v0, x0, body, "period", period), 0, dv_max, 10).

  local v1_horiz to v0_horiz.
  set v1_horiz:mag to v0_horiz:mag + mag.

  local v1 to v1_horiz + v0_vert.

  set n to node_from_vector(dt, v1 - v0).

  return n.
}

function node_ap_time {
  parameter dt.
  parameter ap.

  local v0 to v_at(ship, dt).
  local x0 to x_at(ship, dt).
  local o to orbitat(ship, time:seconds + dt).
  local v0_horiz to vxcl(x0:normalized, v0).
  local v0_vert to vdot(v0, x0:normalized) * x0:normalized.

  if ap < ( x0:mag - o:body:radius )
    set ap to x0:mag.

  local dv_max to 10000. // FIXME: expand_range with f = 2

  if ap < o:apoapsis {
    set dv_max to - v0_horiz:mag.
  }

  local mag to secant_method(guess@:bind(v0, x0, body, "ap", ap), 0, dv_max, 10).

  local v1_horiz to v0_horiz.
  set v1_horiz:mag to v0_horiz:mag + mag.

  local v1 to v1_horiz + v0_vert.

  set n to node_from_vector(dt, v1 - v0).

  return n.
}

function node_pe_time {
  parameter dt.
  parameter pe.

  local v0 to v_at(ship, dt).
  local x0 to x_at(ship, dt).
  local o to orbitat(ship, time:seconds + dt).
  local v0_horiz to vxcl(x0:normalized, v0).
  local v0_vert to vdot(v0, x0:normalized) * x0:normalized.

  if pe > ( x0:mag - o:body:radius )
    set pe to x0:mag.

  local dv_max to 10000. // FIXME: expand_range with f = 2

  if pe < o:periapsis {
    set dv_max to - v0_horiz:mag.
  }

  local mag to secant_method(guess@:bind(v0, x0, body, "pe", pe), 0, dv_max, 10).

  local v1_horiz to v0_horiz.
  set v1_horiz:mag to v0_horiz:mag + mag.

  local v1 to v1_horiz + v0_vert.

  set n to node_from_vector(dt, v1 - v0).

  return n.
}

//////////
// hohmann
//////////


// finds the first hohmann transfer after dt seconds
// dt allows big rockets time to rotate around
function node_hohmann_obt {
  parameter obt.
  parameter dt is 0.

  local sma0 to orbit:semimajoraxis.
  local sma1 to obt:semimajoraxis.

  local h_ang to (1 - (sqrt((sma0/sma1 + 1)^3))/(2*sqrt(2))) * constant:pi * constant:radtodeg.

  function phase_angle {
    parameter dt.

    local x0 to x_at(ship, dt).
    local v0 to v_at(ship, dt).

    local x1 to obt:positionat(time:seconds + dt) - ship:body:position.

    local n to vcrs(x0, v0):normalized.

    local dot to vdot(x0, x1).
    local det to vdot(n, vcrs(x0, x1)).

    return clamp_degrees_360(arctan2(det, dot)).
  }

  function guess {
    parameter dt.
    return phase_angle(dt) - h_ang.
  }

  print "about to expand range".

  local dt_hi to expand_range(guess@, dt, dt + 60).

  print "dt: " + dt.
  print "dt_hi: " + dt_hi.
  print "guess(dt): " + guess(dt).
  print "guess(dt_hi): " + guess(dt_hi).

  print "about to secant_method".

  // BUG: this has issues with the arctan2 result being discontinuous
  local node_dt to secant_method(guess@, dt, dt_hi, 0.1).

  print "done with secant_method".

  // FIXME: could we use the slopes at both ends to determine if the prior problem will fail?
  if secant_method_failed {
    print "secant method failed, trying next patch".
    local dt_hi2 to expand_range(guess@, dt_hi, dt_hi + 60).
    set node_dt to secant_method(guess@, dt_hi, dt_hi2, 0.1).
    print "done with second secant_method".
  }

  set dv to sqrt(body:mu / sma0) * ( sqrt(2 * sma1 / ( sma0 + sma1 ) ) - 1 ).

  print "dv = " + dv.
  print "dt = " + node_dt.

  set v to v_at(ship, node_dt):normalized * dv.

  set n to node_from_vector(node_dt, v).

  return n.
}

//////////////
// inclination
//////////////

// negative inclination value are the expensive burn
function node_inclination_time {
  parameter dt.
  parameter inclination.

  local v0 to v_at(ship, dt).
  local x0 to x_at(ship, dt).
  local lat to lat_at(ship, dt).

  local v0_horiz to vxcl(x0:normalized, v0).

  local v0_vert to vdot(v0, x0:normalized) * x0:normalized.

  local v1_heading to heading_for_inclination(inclination, lat).

  local v1_horiz to angleaxis(v1_heading, x0) * north_at(ship, dt).

  set v1_horiz:mag to v0_horiz:mag.

  local v1 to v0_vert + v1_horiz.

  local n to node_from_vector(dt, v1 - v0).

  return n.
}

function node_inclination_target {
  // FIXME
}

